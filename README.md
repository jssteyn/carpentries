## Carpentries 3 minute lesson

Assumptions:
* Lessons 1, 2 and 3 have been completed
* We will be doing the first three minutes of Lesson 4: Tracking Changes
* Our exercises have been done using Windows Git Bash
* We are working on drive D: which, in Git Bash, is /d/Carpentries/carpentries

![Screenshot](Screenshot1.png)

## Overview

Teaching: 3 minutes
Lesson starts at: Where Are My Changes?
Follow lesson [here](./index.html)
![Screenshot](Screenshot2.png)
